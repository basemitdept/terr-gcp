terraform {
  required_providers {
   
    helm = {
       source = "hashicorp/helm"

    }

    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
    }
  }
}












provider "google" {
  project = "terraform-426822"
  credentials = "${file("terraform-426822-bb037f7c58b5.json")}"
  region = "us-central1"
  zone = "asia-east1-a"
  

}







provider "helm" {
  kubernetes {



   host     = "https://${google_container_cluster.my-gke-cluster.endpoint}"
   
   token                  = data.google_client_config.default.access_token
  
   cluster_ca_certificate = base64decode(google_container_cluster.my-gke-cluster.master_auth.0.cluster_ca_certificate)
  }
}

data "google_client_config" "default" {}
provider "kubernetes" {
host     = "https://${google_container_cluster.my-gke-cluster.endpoint}"
   
   token                  = data.google_client_config.default.access_token
  
   cluster_ca_certificate = base64decode(google_container_cluster.my-gke-cluster.master_auth.0.cluster_ca_certificate)

  
}

provider "kubectl" {
host     = "https://${google_container_cluster.my-gke-cluster.endpoint}"
   
   token                  = data.google_client_config.default.access_token
  
   cluster_ca_certificate = base64decode(google_container_cluster.my-gke-cluster.master_auth.0.cluster_ca_certificate)
   load_config_file       = false

  
}